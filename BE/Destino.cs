﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Destino
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private string ciudad;

        public string Ciudad
        {
            get { return ciudad; }
            set { ciudad = value; }
        }

       private List<Destino> destinos = new List<Destino>();

        public List<Destino> Destinos
        {
            get { return destinos; }
            set { destinos = value; }
        }


        public override string ToString()
        {
            return ID + "" + Ciudad;
        }


    }
}

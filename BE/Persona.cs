﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Persona
    {

        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }


        private string nombre;

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        private string dni;

        public string Dni
        {
            get { return dni; }
            set { dni = value; }
        }


        public override string ToString()
        {
            return ID + "" + Dni;
        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Vuelo
    {

        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private int cant_asientos;

        public int Cant_Asientos
        {
            get { return cant_asientos; }
            set { cant_asientos = value; }
        }

        private DateTime llegada;

        public DateTime Llegada
        {
            get { return llegada; }
            set { llegada = value; }
        }

        private DateTime salida;

        public DateTime Salida
        {
            get { return salida; }
            set { salida = value; }
        }

        private Destino destino;

        public Destino Destino
        {
            get { return destino; }
            set { destino = value; }
        }

        private Persona persona;

        public Persona Persona
        {
            get { return persona; }
            set { persona = value; }
        }


    }
}

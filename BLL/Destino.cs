﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;

namespace BLL
{
    public class Destino
    {
        DAL.MP_Destino MP_Destino = new DAL.MP_Destino();
        public void insertar(BE.Destino destino)
        {
            if (destino.ID == 0)
            {
                MP_Destino.Insertar(destino);
            }

            else
            {
                MP_Destino.Editar(destino);
            }

        }

        public void Eliminar(BE.Destino destino)
        {
               MP_Destino.Eliminar(destino);
         }

        public List<BE.Destino> Listar()
        {
            return MP_Destino.Listar();
        }


    }
}

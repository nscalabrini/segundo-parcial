﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;

namespace BLL
{
    public class Persona
    {
        MP_Persona mppersona = new MP_Persona();
        public void insertar(BE.Persona persona)
        {
            if (persona.ID == 0)
            {
                mppersona.Insertar(persona);

            }

            else
            {
                mppersona.Editar(persona);

            }

        }

        public void Eliminar(BE.Persona persona)
        {

            mppersona.Eliminar(persona);


        }

        public List<BE.Persona> Listar()
        {
            return mppersona.Listar();
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BE;
namespace BLL
{
    public class Vuelo
    {
        MP_Vuelo mpvuelo = new MP_Vuelo();
        public void insertar(BE.Vuelo VN)
        {
            if (VN.Id == 0)
            {
                mpvuelo.Insertar(VN);
            }
        }
        public void Eliminar(BE.Vuelo VN)
        {
            mpvuelo.Eliminar(VN);
        }

        public List<BE.Vuelo> Listar()
        {
            return mpvuelo.Listar();
        }

    }
}

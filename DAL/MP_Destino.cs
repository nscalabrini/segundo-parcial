﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;

namespace DAL
{
    public class MP_Destino
    {
        Acceso acceso = new Acceso();

        public void Insertar(BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@ciudad", destino.Ciudad));
            acceso.Escribir("Insertar_Destino", parametros);
            acceso.Cerrar();

        }

        public void Editar(BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@ID", destino.ID));
            parametros.Add(acceso.CrearParametro("@Ciudad", destino.Ciudad));
            acceso.Escribir("Modificar_Destino", parametros);
            acceso.Cerrar();



        }

        public void Eliminar(BE.Destino destino)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@ID", destino.ID));
            acceso.Escribir("Borrar_Destino", parametros);
            acceso.Cerrar();


        }

        public List<BE.Destino> Listar()
        {
            List<BE.Destino> destinos = new List<BE.Destino>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Destino");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Destino destino = new BE.Destino();
                destino.ID = int.Parse(registro[0].ToString());
                destino.Ciudad = registro[1].ToString();
                destinos.Add(destino);
            }
            return destinos;
        }




    }
}

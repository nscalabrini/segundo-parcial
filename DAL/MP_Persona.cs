﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;

namespace DAL
{
    public class MP_Persona
    {
        Acceso acceso = new Acceso();

        public void Insertar(BE.Persona pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@dni", pasajero.Dni));
            parametros.Add(acceso.CrearParametro("@nombre", pasajero.Nombre));
            acceso.Escribir("Insertar_Persona", parametros);
            acceso.Cerrar();

        }

        public void Editar(BE.Persona pasajero)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@ID", pasajero.ID));
            parametros.Add(acceso.CrearParametro("@dni", pasajero.Dni));
            parametros.Add(acceso.CrearParametro("@nombre", pasajero.Nombre));
            acceso.Escribir("Modificar_Persona", parametros);
            acceso.Cerrar();



        }

        public void Eliminar(BE.Persona persona)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@ID", persona.ID));
            acceso.Escribir("Borrar_Persona", parametros);
            acceso.Cerrar();


        }

        public List<BE.Persona> Listar()
        {
            List<BE.Persona> personas = new List<BE.Persona>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Persona");
            acceso.Cerrar();

            foreach (DataRow registro in tabla.Rows)
            {
                BE.Persona persona = new BE.Persona();
                persona.ID = int.Parse(registro[0].ToString());
                persona.Dni = registro[1].ToString();
                persona.Nombre = registro[2].ToString();


                personas.Add(persona);
            }


            return personas;





        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class MP_Vuelo
    {
        Acceso acceso = new Acceso();

        public void Insertar(BE.Vuelo vuelo)
        {

            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@Llegada", vuelo.Llegada));
            parametros.Add(acceso.CrearParametro("@Salida", vuelo.Salida));
            parametros.Add(acceso.CrearParametro("@Cant_asientos", vuelo.Cant_Asientos));
            parametros.Add(acceso.CrearParametro("@Destino", vuelo.Destino.ToString()));
            parametros.Add(acceso.CrearParametro("@Persona", vuelo.Persona.ToString()));
            acceso.Abrir();
            acceso.Escribir("insertar_Vuelo", parametros);
            acceso.Cerrar();

        }



        public void Eliminar(BE.Vuelo vuelo)
        {
            acceso.Abrir();
            List<IDbDataParameter> parametros = new List<IDbDataParameter>();
            parametros.Add(acceso.CrearParametro("@ID", vuelo.Id));
            acceso.Escribir("Borrar_Vuelo", parametros);
            acceso.Cerrar();


        }

        public List<BE.Vuelo> Listar()
        {
            List<BE.Vuelo> vuelos = new List<BE.Vuelo>();
            Acceso acceso = new Acceso();
            acceso.Abrir();
            DataTable tabla = acceso.Leer("Listar_Vuelo");
            acceso.Cerrar();


            foreach (DataRow registro in tabla.Rows)
            {
                BE.Vuelo vuelo = new BE.Vuelo();
                BE.Destino destino = new BE.Destino();

                vuelo.Id = int.Parse(registro[0].ToString());
                vuelo.Llegada = (DateTime)registro[1];
                vuelo.Salida = (DateTime)registro[2];
                vuelo.Cant_Asientos = int.Parse(registro[3].ToString());

                vuelos.Add(vuelo);
            }


            return vuelos;
        }

    }
}

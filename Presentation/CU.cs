﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class CU : UserControl
    {
        public CU()
        {
            InitializeComponent();
        }

        private bool requerido;

        public bool Requerido
        {
            get { return requerido; }
            set { requerido = value; }
        }


        public string Etiqueta
        {
            get { return label1.Text; }

            set
            {

                label1.Text = value;
            }
        }


        public string Valor()
        {

            string valor;

            return valor = textBox1.Text;

        }


        public bool Validar()
        {


            bool ok = true;

            if (requerido)
            {
                if (string.IsNullOrWhiteSpace(textBox1.Text))
                {
                    textBox1.BackColor = Color.Gray;
                }
                else
                {
                    textBox1.BackColor = Color.White;

                }

            }


            return ok;
        }



        private void CU_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class frmdtno : Form
    {
        BLL.Destino gestordestino = new BLL.Destino();

        public frmdtno()
        {
            InitializeComponent();
        }

        private void frmdtno_Load(object sender, EventArgs e)
        {
            enlazar();
        }

        public void enlazar()
        {

            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestordestino.Listar();


        }



        private void button1_Click(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is CU)
                {

                    ((CU)c).Validar();
                }

            }

            BE.Destino destino = new BE.Destino();

           destino.Ciudad = cu1.Valor();
           gestordestino.insertar(destino);
           enlazar();
                    

        }

        private void cu1_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is CU)
                {

                    ((CU)c).Validar();
                }

            }

            BE.Destino destino = new BE.Destino();
            destino.ID = int.Parse(cu2.Valor());
            gestordestino.insertar(destino);
            enlazar();


        }

        private void button3_Click(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is CU)
                {

                    ((CU)c).Validar();
                }

            }

            BE.Destino destino = new BE.Destino();
            destino.ID = int.Parse(cu2.Valor());
            destino.Ciudad = cu1.Valor();
            gestordestino.insertar(destino);
            enlazar();
            destino.Destinos.Add(destino);



        }
    }
}

﻿namespace Presentation
{
    partial class frmper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cu1 = new Presentation.CU();
            this.cu2 = new Presentation.CU();
            this.cu3 = new Presentation.CU();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn1
            // 
            this.btn1.Location = new System.Drawing.Point(672, 32);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(149, 65);
            this.btn1.TabIndex = 3;
            this.btn1.Text = "Crear";
            this.btn1.UseVisualStyleBackColor = true;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Location = new System.Drawing.Point(672, 117);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(149, 65);
            this.btn2.TabIndex = 4;
            this.btn2.Text = "Borrar";
            this.btn2.UseVisualStyleBackColor = true;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.Location = new System.Drawing.Point(672, 209);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(149, 65);
            this.btn3.TabIndex = 5;
            this.btn3.Text = "Modificar";
            this.btn3.UseVisualStyleBackColor = true;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 324);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1074, 150);
            this.dataGridView1.TabIndex = 6;
            // 
            // cu1
            // 
            this.cu1.Etiqueta = "Nombre";
            this.cu1.Location = new System.Drawing.Point(46, 32);
            this.cu1.Name = "cu1";
            this.cu1.Requerido = false;
            this.cu1.Size = new System.Drawing.Size(495, 61);
            this.cu1.TabIndex = 7;
            // 
            // cu2
            // 
            this.cu2.Etiqueta = "DNI";
            this.cu2.Location = new System.Drawing.Point(46, 121);
            this.cu2.Name = "cu2";
            this.cu2.Requerido = false;
            this.cu2.Size = new System.Drawing.Size(495, 61);
            this.cu2.TabIndex = 8;
            // 
            // cu3
            // 
            this.cu3.Etiqueta = "ID";
            this.cu3.Location = new System.Drawing.Point(46, 209);
            this.cu3.Name = "cu3";
            this.cu3.Requerido = false;
            this.cu3.Size = new System.Drawing.Size(495, 61);
            this.cu3.TabIndex = 9;
            // 
            // frmper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1133, 777);
            this.Controls.Add(this.cu3);
            this.Controls.Add(this.cu2);
            this.Controls.Add(this.cu1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Name = "frmper";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frmper_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private CU cu1;
        private CU cu2;
        private CU cu3;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
using BE;

namespace Presentation
{
    public partial class frmper : Form
    {

        BLL.Persona gestorpersona = new BLL.Persona();

        public frmper()
       
        
        {
            InitializeComponent();
        }
       

        private void frmper_Load(object sender, EventArgs e)
        {
            enlazar();
        }

        public void enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorpersona.Listar();

        }


        private void btn1_Click(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is CU)
                {

                    ((CU)c).Validar();
                }

            }

            BE.Persona per = new BE.Persona();

            per.Nombre = cu1.Valor();
            per.Dni = cu2.Valor();
           

            gestorpersona.insertar(per);
            enlazar();

        }

        private void btn2_Click(object sender, EventArgs e)
        {

            foreach (Control c in this.Controls)
            {
                if (c is CU)
                {

                    ((CU)c).Validar();
                }

            }

            BE.Persona per = new BE.Persona();
            per.ID = int.Parse(cu3.Valor());
            gestorpersona.Eliminar(per);
            enlazar();

        }

        private void btn3_Click(object sender, EventArgs e)
        {
            foreach (Control c in this.Controls)
            {
                if (c is CU)
                {

                    ((CU)c).Validar();
                }

            }

            BE.Persona per = new BE.Persona();
            per.ID = int.Parse(cu3.Valor());
            per.Dni = cu2.Valor();
            per.Nombre = cu1.Valor();

            gestorpersona.insertar(per);
            enlazar();
        }
    }
}

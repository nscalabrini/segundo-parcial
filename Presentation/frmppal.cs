﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class frmppal : Form
    {
        public frmppal()
        {
            InitializeComponent();
        }

        private void personasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmper frm = new frmper();
            frm.Show();
        }

        private void destinosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmdtno frm = new frmdtno();
            frm.Show();
        }

        private void vuelosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmvlo frm = new frmvlo();
            frm.Show();
        }

        private void datosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmdtos frm = new frmdtos();

            frm.Show();
        }
    }
}

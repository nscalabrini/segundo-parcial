﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentation
{
    public partial class frmvlo : Form
    {
        public frmvlo()
        {
            InitializeComponent();
        }

        BLL.Vuelo gestorvuelo = new BLL.Vuelo();
        BLL.Destino gestordestino = new BLL.Destino();
        BLL.Persona gestorpersona = new BLL.Persona();

        public void enlazar()
        {
            dataGridView1.DataSource = null;
            dataGridView1.DataSource = gestorvuelo.Listar();


        }






        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void frmvlo_Load(object sender, EventArgs e)
        {
            enlazar();
            comboBox1.DataSource = gestordestino.Listar();
            comboBox2.DataSource = gestorpersona.Listar();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            BE.Vuelo vuelo = new BE.Vuelo();
            vuelo.Llegada = (DateTime)dateTimePicker1.Value.Date;
            vuelo.Salida = (DateTime)dateTimePicker2.Value.Date;
            vuelo.Cant_Asientos = int.Parse(textBox1.Text);
            vuelo.Destino = (BE.Destino)comboBox1.SelectedItem;
            vuelo.Persona= (BE.Persona)comboBox2.SelectedItem;

            gestorvuelo.insertar(vuelo);
            enlazar();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            BE.Vuelo vuelo  = new BE.Vuelo();
            vuelo.Id = int.Parse(textBox1.Text);
            gestorvuelo.Eliminar(vuelo);
            enlazar();
        }
    }
}
